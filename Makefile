#!/usr/bin/make -f
# -*- mode:makefile -*-

TARGET=production

all: $(addprefix $(TARGET)/, $(wildcard *.html))

$(TARGET)/%: % $(shell ./versioned main.css)
	sed "s|main.css|$(word 2, $^)|g" $< > $@

main.%.css: main.css
	cp $< $(TARGET)/$@

clean:
	$(RM) production/* *~
